const encryptionAlgorithm = "aes256";
const passwordEncryptionKey = "HospitalCare2020to2021ProjectEncryptionKeyIsVerryPoor";
const jwtSecretCode = "HospitalCareJWTCodeisNotGood";
const utfVersion = "utf8";
const hex = "hex";
const unknown = "Unknown Error";
const status = "status";
const message = "message";
const acknowledged = "acknowledged";
const adminUserID = "admin_user_id";
const accessToken = "access_token";
const date = "date";
const insertID = "insertedId";

module.exports = {
    enalgo: encryptionAlgorithm,
    psdenkey : passwordEncryptionKey,
    jwtcd : jwtSecretCode,
    h : hex,
    u : utfVersion,
    uke : unknown,
    s : status,
    m : message,
    akn : acknowledged,
    aui : adminUserID,
    ac : accessToken,
    d : date,
    i : insertID
};