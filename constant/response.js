const emailExists = "Using this Email ID an Account Already Created";
const phoneNumberExists = "Using this Phone Number an Account Already Created ";
const usernameExists = "Using this UserName an Account Already Created";
const regSucc = "Registration Successfully";
const databaseError = "Database Error";
const validPassword = "Please enter a valid Password";
const notRegistered = "You are not registered";
const wrongPassword = "Wrong Password";
const accountNotActivated = "Your account is not activated. Contact Admin";
const loginSuccess = "Login Successful";
const gitURLProblem = "Please check the Git URL Not Configured";
const intentJSONNoData = "Intent JSON Data file is blank in git";
const intentParsed = "Intent.json parsed Successfully and updated";
const blankDB = "No Data in Database";
module.exports = {
    ee: emailExists,
    phe: phoneNumberExists,
    ue: usernameExists,
    rs : regSucc,
    dbr : databaseError,
    vp : validPassword,
    nr : notRegistered,
    wp : wrongPassword,
    ana : accountNotActivated,
    ls : loginSuccess,
    g : gitURLProblem,
    i : intentJSONNoData,
    ij : intentParsed,
    b : blankDB

};