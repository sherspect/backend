const active = "active";
const password = "password";
const email = "emailID";
const phoneNumber = "phone_number";
const username = "username";
const id = "_id";
const tag = "tag";
const patterns = "patterns";
const value = "value";
const tagID = tag+id;
const rsp = "responses";
module.exports = {
    act: active,
    pwd: password,
    e: email,
    pn: phoneNumber,
    un: username,
    id : id,
    t : tag,
    p : patterns,
    v : value,
    td : tagID,
    r : rsp
};