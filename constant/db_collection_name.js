const adminUser = "adminUser";
const jwtToken = "jwtToken";
const intents = "tags";
const intents2 = "intents";
const patterns = "patterns";
const response = "intents_response";

module.exports = {
    aU: adminUser,
    jt : jwtToken,
    i : intents,
    p : patterns,
    r : response,
    int : intents2
};
