const MongoClient = require("mongodb").MongoClient;
const DATABASE_NAME = "intents";
var mongoURI = "";
var database = "";

function connectToMongo() {
   mongoURI = "mongodb://localhost:27017/?readPreference=primary&appname=MongoDB%20Compass&directConnection=true&ssl=false";
   // mongoURI = "";
	MongoClient.connect(mongoURI, { useNewUrlParser: true }, (error, client) => {
        if (error) {
            throw error;
        }
        database = client.db(DATABASE_NAME);
        console.log("Connected to ab `" + DATABASE_NAME + "`!");
    });
    return database;
}

async function getAllData(collectionName) {
    collection = database.collection(collectionName);
    const cursor = collection.find();
    const allValues = await cursor.toArray();
    return allValues;
}


async function getLastData(collectionName) {
    collection = database.collection(collectionName);
    const cursor = collection.find().sort({$natural: -1}).limit(1);
    const allValues = await cursor.toArray();
    return allValues;
}


async function getData(collectionName, query) {
    collection = database.collection(collectionName);
    const cursor = collection.find(query);
    const allValues = await cursor.toArray();
    return allValues;
}

async function getCount(collectionName, query) {
    collection = database.collection(collectionName);
    const cursor = collection.find(query).count();
    return cursor;
}

async function insertOneData(collectionName, data) {
    collection = database.collection(collectionName);
    const result = await collection.insertOne(data);
    return result;
}


async function updateOneData(collectionName, where, data) {
    // this option instructs the method to create a document if no documents match the filter
    const options = { upsert: true };
    const updateDoc = {
        $set: data
    };
    collection = database.collection(collectionName);
    const result = await collection.updateOne(where, updateDoc, options);
    return result;
}

async function updateManyData(collectionName, where, data) {
    // this option instructs the method to create a document if no documents match the filter
    const options = { upsert: true };
    const updateDoc = {
        $set: data
    };
    collection = database.collection(collectionName);
    const result = await collection.updateMany(where, updateDoc, options);
    return result;
}

async function replaceOneData(collectionName, where, data) {
    // This Method will replace the dictionay, be careful to using of it. 
    // this option instructs the method to create a document if no documents match the filter
    const options = { upsert: true };
    collection = database.collection(collectionName);
    const result = await collection.replaceOne(where, data, options);
    return result;
}

async function deleteOneData(collectionName, where) {
    collection = database.collection(collectionName);
    const result = await collection.deleteOne(where);
    return result;
}

async function deleteAll(collectionName, where) {
    collection = database.collection(collectionName);
    const result = await collection.deleteMany(where);
    return result;
}

module.exports = { getCount, getLastData, getAllData, getData, insertOneData, connectToMongo, updateOneData, deleteOneData, deleteAll, updateManyData }

