var crypto = require('crypto');
var jwt = require('jsonwebtoken');
const fs = require('fs')
const gc = require('../constant/general_constants.js');


async function verifyToken(req) {
    // Get the user from the jwt token and add id to req object
    const token = req.header('auth-token');
    if (!token) {
        return please;
    }
    try {
        const data = jwt.verify(token, gc.jwtcd);
        return data;
    } catch (error) {
        return please;
    }

}
async function generateToken(data) {
    const authtoken = jwt.sign(data, gc.jwtcd);
    return authtoken;
}
async function encrypt(string) {
    var cipher = crypto.createCipher(gc.enalgo, gc.psdenkey);
    var encrypted = cipher.update(string, gc.u, gc.h) + cipher.final(gc.h);
    return encrypted;
}
async function decrypt(string) {
    var decipher = crypto.createDecipher(gc.enalgo, gc.psdenkey);
    var decrypted = decipher.update(string, gc.h, gc.u) + decipher.final(gc.u);
    return decrypted;
}



function log() {
    const content = 'Some content! \n'
    var logpath = ""
        
        logpath = "D:/logs"
        mkdir(logpath)
        logpath = logpath + "/gaschecker"
        mkdir(logpath)
    
    logpath = logpath + "/register.txt"


    fs.writeFile(logpath, content, { flag: 'a+' }, err => {
        if (err) {
            console.error(err)
            return
        }
       
    })
}

function mkdir(logpath) {
    if (!fs.existsSync(logpath)) {
        fs.mkdirSync(logpath);
    }
}

module.exports = { log, encrypt, decrypt, generateToken, verifyToken }