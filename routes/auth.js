const Express = require("express");
const dbOp = require("../database/db");
const dbConstant = require("../constant/db_collection_name");
const dbDocConstant = require("../constant/db_collection_document_key_name");
const rsp = require("../constant/response");
const gf = require("../generalFunctions/general_functions");
const gc = require("../constant/general_constants");
const env = require("../envConfig/env");
const git = require("../thirdPartyAPICall/git");
const cors = require('cors');
const router = Express();
// CORS is enabled for all origins
router.use(cors());
const BodyParser = require("body-parser");
router.use(BodyParser.json());
router.use(BodyParser.urlencoded({ extended: true }));

router.get("", async (req, res) => {
  const finalRes = { status: true, message: "Test Website " };
  res.json(finalRes);
});

router.get("/getIntentsData", async (req, res) => {
  var response = {};
  var mainResponse = [];

  var query = {};
  var status = true;
  query[dbDocConstant.act] = status;
  // Fetch Intents
  var result = await dbOp.getData(dbConstant.i, query);

  if (result.length != 0) {
    for (var i = 0; i < result.length; i++) {
      response = {};
      response[dbDocConstant.t] = result[i][dbDocConstant.t];
      env.cl("response Checking  " + JSON.stringify(response));
      var query2 = {};
      query2[dbDocConstant.act] = status;
      query2[dbDocConstant.td] = { $all: [result[i][dbDocConstant.id]] };
      // Fetch Patterns
      var result2 = await dbOp.getData(dbConstant.p, query2);
      var array1 = [];
      if (result2.length != 0) {
        for (var j = 0; j < result2.length; j++) {
          array1[j] = result2[j][dbDocConstant.p];
        }
      }
      response[dbDocConstant.p] = array1;
      var query3 = {};
      query3[dbDocConstant.act] = status;
      query3[dbDocConstant.td] = { $all: [result[i][dbDocConstant.id]] };
      // Fetch Response of Intents
      var result3 = await dbOp.getData(dbConstant.r, query3);
      var array2 = [];
      if (result3.length != 0) {
        for (var k = 0; k < result3.length; k++) {
          array2[k] = result3[k][dbDocConstant.v];
        }
      }
      response[dbDocConstant.r] = array2;
      mainResponse[i] = response;
      env.cl("mainResponse Checking  " + JSON.stringify(mainResponse));
    }
  } else {
    status = false;
    response[gc.s] = status;
    response[gc.m] = rsp.b;
    res.send(response);
  }
  response = {};
  response[dbConstant.int] = mainResponse;
  res.send(response);
});

router.get("/insertIntenetJSONFile", async (req, res) => {
  try {
    const responseGit = await git.intentsJson();
    var status = false;
    var response = {};
    // Start Parsing intents
    if (responseGit.length != 0) {
      for (var i = 0; i < responseGit.data.intents.length; i++) {
        // Start Parsing tag
        var query = {};
        var intentDBID = "";
        query[dbDocConstant.t] = responseGit.data.intents[i].tag;
        var result = await dbOp.getData(dbConstant.i, query);
        if (result.length == 0) {
          // insert
          status = true;
          var insertData = {};
          insertData[dbDocConstant.t] = responseGit.data.intents[i].tag;
          insertData[dbDocConstant.act] = status;
          var trash = await dbOp.insertOneData(dbConstant.i, insertData);
          env.cl("Trash Data in Loop " + JSON.stringify(trash));
          intentDBID = trash[gc.i];
        } else {
          env.cl("intentDBID Data in Loop " + JSON.stringify(result[0]._id));
          intentDBID = result[0]._id;
        }
        env.cl("intentDBID Data in Loop " + intentDBID);

        // Start Parsing patterns
        for (var j = 0; j < responseGit.data.intents[i].patterns.length; j++) {
          env.cl(
            "patterns Data in Loop " + responseGit.data.intents[i].patterns[j]
          );
          var patternDBId = "";
          var query2 = {};
          query2[dbDocConstant.p] = responseGit.data.intents[i].patterns[j];
          var result2 = await dbOp.getData(dbConstant.p, query2);
          env.cl("result2 Data query " + JSON.stringify(query2));
          env.cl("result2 Data count " + result2.length);
          if (result2.length == 0) {
            // insert
            status = true;
            var insertData2 = {};
            var arr = [];
            arr[0] = intentDBID;
            insertData2[dbDocConstant.p] =
              responseGit.data.intents[i].patterns[j];
            insertData2[dbDocConstant.act] = status;
            insertData2[dbDocConstant.td] = arr;
            var trash2 = await dbOp.insertOneData(dbConstant.p, insertData2);
            env.cl("trash2 Data in Loop " + JSON.stringify(trash2));
            patternDBId = trash2[gc.i];
          } else {
            //update

            for (var k = 0; k < result2.length; k++) {
              // env.cl("patern ID Result from Database Data in Loop " + JSON.stringify(result2[k][dbDocConstant.td]));
              var insertNewintentID = true;
              for (var l = 0; l < result2[k][dbDocConstant.td].length; l++) {
                env.cl(
                  "patern ID Result from Database Data in Loop " +
                    JSON.stringify(result2[k][dbDocConstant.td][l])
                );
                var arr2 = [];

                if (
                  JSON.stringify(result2[k][dbDocConstant.td][l]) ==
                  JSON.stringify(intentDBID)
                ) {
                  env.cl("got it ");
                  insertNewintentID = false;
                  arr2[l] = result2[k][dbDocConstant.td][l];
                } else {
                  env.cl("not matched  ");
                }

                if (insertNewintentID) {
                  arr2[result2[k][dbDocConstant.td].length] = intentDBID;
                  //arr2[result2[k][dbDocConstant.td].length] = "TestID";
                  var query3 = {};
                  var update3 = {};
                  query3[dbDocConstant.id] = result2[k][dbDocConstant.id];
                  update3[dbDocConstant.td] = arr2;
                  await dbOp.updateOneData(dbConstant.p, query3, update3);
                }
                //env.cl("After Adding Test ID "+ JSON.stringify(arr2) );
                //env.cl("Object ID to update  "+ JSON.stringify(result2[k][dbDocConstant.id]) );
              }
            }
          }
          env.cl("patern ID Data in Loop " + patternDBId);
        }
        // Start Parsing responses
        env.cl(
          "Before parsing responses intentDBID Data in Loop " + intentDBID
        );
        for (var m = 0; m < responseGit.data.intents[i].responses.length; m++) {
          env.cl(
            "response Data in Loop " + responseGit.data.intents[i].responses[m]
          );

          var query5 = {};
          var arr5 = [];
          arr5[0] = intentDBID;
          query5[dbDocConstant.v] = responseGit.data.intents[i].responses[m];
          var result5 = await dbOp.getData(dbConstant.r, query5);
          if (result5.length == 0) {
            // insert
            status = true;
            var insertData5 = {};
            insertData5[dbDocConstant.v] =
              responseGit.data.intents[i].responses[m];
            insertData5[dbDocConstant.act] = status;
            insertData5[dbDocConstant.td] = arr5;
            var trash = await dbOp.insertOneData(dbConstant.r, insertData5);
          } else {
            for (var k = 0; k < result5.length; k++) {
              // env.cl("patern ID Result from Database Data in Loop " + JSON.stringify(result2[k][dbDocConstant.td]));
              var insertNewintentID = true;
              for (var l = 0; l < result5[k][dbDocConstant.td].length; l++) {
                env.cl(
                  "patern ID Result from Database Data in Loop " +
                    JSON.stringify(result5[k][dbDocConstant.td][l])
                );
                var arr2 = [];

                if (
                  JSON.stringify(result5[k][dbDocConstant.td][l]) ==
                  JSON.stringify(intentDBID)
                ) {
                  env.cl("got it ");
                  insertNewintentID = false;
                  arr2[l] = result5[k][dbDocConstant.td][l];
                } else {
                  env.cl("not matched  ");
                }

                if (insertNewintentID) {
                  arr2[result5[k][dbDocConstant.td].length] = intentDBID;
                  //arr2[result2[k][dbDocConstant.td].length] = "TestID";
                  var query3 = {};
                  var update3 = {};
                  query3[dbDocConstant.id] = result5[k][dbDocConstant.id];
                  update3[dbDocConstant.td] = arr2;
                  await dbOp.updateOneData(dbConstant.r, query3, update3);
                }
                //env.cl("After Adding Test ID "+ JSON.stringify(arr2) );
                //env.cl("Object ID to update  "+ JSON.stringify(result2[k][dbDocConstant.id]) );
              }
            }
          }
        }
      }
      status = true;
      response[gc.s] = status;
      response[gc.m] = rsp.ij;
      res.json(response);
    } else {
      response[gc.s] = status;
      response[gc.m] = rsp.i;
      res.send(response);
    }
  } catch (error) {
    env.cl("Error Data >>> " + error);
    env.cl("Error Type >>> " + typeof error);
    status = false;
    response[gc.s] = status;
    response[gc.m] = error;
    res.send(response);
  }
});

router.post("/login", async (req, res) => {
  var status = false;
  try {
    var input = req.body;
    var response = {};
    var existUser = false;
    response[gc.m] = "";
    if (
      input[dbDocConstant.pwd] != null &&
      input[dbDocConstant.pwd].length != 0
    ) {
      for (var i = 0; i < 3; i++) {
        var query = {};
        var data = "";
        var result = "";
        if (i == 0) {
          if (input[dbDocConstant.e] != undefined) {
            query[dbDocConstant.e] = input[dbDocConstant.e];
            data = rsp.ee;
          }
        }
        if (i == 1) {
          if (input[dbDocConstant.pn] != undefined) {
            query[dbDocConstant.pn] = input[dbDocConstant.pn];
            data = rsp.phe;
          }
        }
        if (i == 2) {
          if (input[dbDocConstant.un] != undefined) {
            query[dbDocConstant.un] = input[dbDocConstant.un];
            data = rsp.ue;
          }
        }
        env.cl("Select Query >>> " + JSON.stringify(query));
        env.cl("Select Query >>> " + JSON.stringify(query));
        const isEmpty = Object.keys(query).length === 0;
        env.cl("isEmpty  >>> " + isEmpty);
        if (!isEmpty) {
          result = await dbOp.getData(dbConstant.aU, query);
          env.cl(result);
        }
        env.cl("User Count in Database  >>> " + result);
        if (result.length != 0) {
          existUser = true;
          break;
        }
      }
      if (existUser) {
        var encryptedPassword = await gf.encrypt(input[dbDocConstant.pwd]);
        env.cl("encryptedPassword  >>> " + encryptedPassword);
        if (result[0][dbDocConstant.pwd] == encryptedPassword) {
          if (!result[0][dbDocConstant.act]) {
            response[gc.s] = status;
            response[gc.m] = rsp.ana;
            res.json(response);
          } else {
            var tokenData = {};
            tokenData[gc.aui] = result[0][dbDocConstant.id];
            env.cl("token Data   >>> " + JSON.stringify(tokenData));
            const jstToken = await gf.generateToken(tokenData);
            env.cl("token    >>> " + jstToken);
            const date = new Date();
            status = true;
            var tokenDataInsert = {};
            tokenDataInsert[gc.ac] = jstToken;
            tokenDataInsert[gc.aui] = result[0][dbDocConstant.id];
            tokenDataInsert[gc.s] = status;
            tokenDataInsert[gc.d] = date;
            await dbOp.insertOneData(dbConstant.jt, tokenDataInsert);

            response[gc.s] = status;
            response[gc.m] = rsp.ls;
            response[gc.ac] = jstToken;
            res.json(response);
          }
        } else {
          response[gc.s] = status;
          response[gc.m] = rsp.wp;
          res.json(response);
        }
      } else {
        response[gc.s] = status;
        response[gc.m] = rsp.nr;
        res.json(response);
      }
    } else {
      response[gc.s] = status;
      response[gc.m] = rsp.vp;
      res.json(response);
    }
  } catch (error) {
    env.cl("Error Data >>> " + error);
    env.cl("Error Type >>> " + typeof error);
    status = false;
    error[gc.s] = status;
    res.status(200).send(error);
  }
});

router.put("/register", async (req, res) => {
  var status = false;
  try {
    var input = req.body;
    var response = {};
    var existUser = false;
    response[gc.m] = "";
    for (var i = 0; i < 3; i++) {
      var query = {};
      var data = "";
      if (i == 0) {
        query[dbDocConstant.e] = input[dbDocConstant.e];
        data = rsp.ee;
        env.cl("Undefined Data Checking >>> " + data);
      }
      if (i == 1) {
        query[dbDocConstant.pn] = input[dbDocConstant.pn];
        data = rsp.phe;
      }
      if (i == 2) {
        query[dbDocConstant.un] = input[dbDocConstant.un];
        data = rsp.ue;
      }
      env.cl("Select Query >>> " + JSON.stringify(query));
      var count = await dbOp.getCount(dbConstant.aU, query);
      env.cl("User Count in Database  >>> " + count);
      if (count != 0) {
        existUser = true;
        if (response[gc.m] != "") {
          response[gc.m] = response[gc.m] + " and " + data;
        } else {
          response[gc.m] = data;
        }
      }
    }
    response[gc.s] = status;
    env.cl("Response Data >>> " + JSON.stringify(response));
    if (existUser) {
      res.json(response);
    } else {
      var encryptedPassword = await gf.encrypt(req.body.password);
      input[dbDocConstant.act] = false;
      input[dbDocConstant.pwd] = encryptedPassword;
      env.cl("inserted Data >>> " + JSON.stringify(input));
      insertData = await dbOp.insertOneData(dbConstant.aU, input);
      if (insertData[gc.akn]) {
        status = true;
        response[gc.m] = rsp.rs;
      } else {
        response[gc.m] = rsp.dbr;
      }
      response[gc.s] = status;
      res.json(response);
    }
  } catch (error) {
    env.cl("Error Data >>> " + error);
    env.cl("Error Type >>> " + typeof error);
    status = false;
    error[gc.s] = status;
    res.status(200).send(error);
  }
});

module.exports = router;
