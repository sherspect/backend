const axios = require("axios");
var baseURL = "https://bitbucket.org/";

module.exports = {
   intentsJson: ()  => axios({
    method: "GET",
    url: baseURL + "sherspect/ml/raw/ed566b5f3fcaec625e20838eabd4151288111c91/intents.json",
    headers: {
      "Accept": "*/*",
      "User-Agent": "Thunder Client (https://www.thunderclient.io)"
   }  
    
 }),

}